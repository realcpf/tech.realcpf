---
layout: ../layouts/AboutLayout.astro
title: "猫系小狗"
---

`日常单纯小狗,不时别扭喵喵`<br>

热衷于大数据组件集成组合、基于pingora编写流量网管接入k8s gateway、各种agent hock工具
<br>

1. 大多数的（非hadoop系）的打数据组件集成到client通过sql调用如<br>

```sql
create source pg1 with (
    `engine`='seatunnel',
    'jdbc.url' = "jdbc:postgresql://localhost:5432/test"
    'jdbc.driver' = "org.postgresql.Driver"
    'jdbc.user' = "root"
    'jdbc.password' = "test"
    'jdbc.query' = "select * from source limit 16"
)
```

2. [基于pingora的流量网管](/posts/pingora-simple/)
3. 各种agent、hock工具，比如arthas、java agent等

最近沉迷网络小说<br>

- 杂货店禁止驯养饿虎
- 我们消失的那一年
- 白马与天涯
- 致我那菜市场的白月光
- 在异世界扫垃圾

最后，[联系工作](mailto:realcpf@163.com?subject=NiceToMeetYou&body=dear%20jc.liu)

```

```
