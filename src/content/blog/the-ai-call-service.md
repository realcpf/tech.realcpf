---
author: jiacheng.liu
pubDatetime: 2025-02-01T09:16:00.000Z
modDatetime:
title: 关于大模型调用服务里不得不说的那些事
featured: false
draft: false
tags:
  - framework
description: 关于大模型调用服务里不得不说的那些事
---

下面这段1、2、3是通过DeekSeek总结的，代码部分人工作了调整，为了保证可以运行：

---

### **1. 智能Web服务的趋势**

#### **从功能型到认知型的演进**

- **传统Web服务**：以数据查询、表单提交为主（如电商下单、信息展示）。
- **智能Web服务**：借助大模型实现自然语言交互、个性化推荐、动态内容生成。
  - **核心能力**：
    - **语义理解**：用户意图识别（如模糊搜索、情感分析）。
    - **内容生成**：自动生成文本、代码、图片（如客服机器人、设计助手）。
    - **决策支持**：基于数据分析的智能决策（如金融风控、医疗诊断建议）。

#### **典型场景**

- **自动化流程**：合同审核、报告生成。
- **多模态交互**：语音输入、图像识别与文本联动。
- **实时个性化**：根据用户历史行为动态调整服务逻辑。

---

### **2. 大模型服务生态性**

#### **生态分层**

| 层级           | 代表服务/工具               | 作用                   |
| -------------- | --------------------------- | ---------------------- |
| **基础设施层** | OpenAI API、Claude、Llama 2 | 提供基础模型能力       |
| **框架工具层** | LangChain、Hugging Face     | 简化模型调用与流程编排 |
| **应用集成层** | Spring AI、FastAPI插件      | 与现有开发框架深度整合 |

#### **开发者友好性**

- **标准化接口**：RESTful API、SDK封装（如OpenAI Java库）。
- **本地化部署**：Ollama支持本地运行大模型，降低数据泄露风险。
- **成本分级**：按需选择云API（按Token计费）或私有化部署。

---

### **3. Java开发：Spring Boot的AI Starter调用大模型**

#### **Spring-ai项目简介**

- **定位**：Spring官方推出的AI集成框架，统一不同模型服务的调用方式。
- **核心功能**：
  - 支持OpenAI、Ollama、Hugging Face等主流服务商。
  - 提供模板化接口（如`ChatClient`、`EmbeddingClient`）。

#### **开发步骤**

1. **添加依赖**：

   ```xml
   <!-- 主要是AI部分的
           <dependency>
            <groupId>org.springframework.ai</groupId>
            <artifactId>spring-ai-ollama-spring-boot-starter</artifactId>
        </dependency> -->
   <!-- pom.xml -->
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>3.4.2</version>
        <relativePath/>
    </parent>
    <groupId>tech.realcpf</groupId>
    <artifactId>jc-ai-services</artifactId>
    <version>1.0-SNAPSHOT</version>

    <properties>
        <java.version>17</java.version>
        <spring-ai.version>1.0.0-M5</spring-ai.version>
    </properties>
    <dependencies>
    <!-- ai部分的库 -->
        <dependency>
            <groupId>org.springframework.ai</groupId>
            <artifactId>spring-ai-ollama-spring-boot-starter</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
    </dependencies>


    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.ai</groupId>
                <artifactId>spring-ai-bom</artifactId>
                <version>${spring-ai.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

   ```

2. **配置模型参数**：

   ```yaml
   # application.properties
   spring.ai.ollama.base-url=http://localhost:11434
   spring.ai.ollama.chat.model=deepseek-r1:8b
   ```

3. **调用模型服务**：

   ```java
   @RestController
   public class TranslationController {
       @Autowired
       private OllamaChatModel ollamaChatModel;

       @PostMapping("/translate")
       public String translate(@RequestParam String text,
                               @RequestParam String targetLang) {
           String prompt = "将以下文本翻译为" + targetLang + ":\n" + text;
           return ollamaChatModel.call(prompt);
       }
   }
   ```

---

### **4. 翻译Demo完整示例**

#### **环境准备**

1. 安装Ollama（本地模型服务）：
   ```bash
   curl -fsSL https://ollama.com/install.sh | sh
   ollama pull deepseek-r1:8b  # 下载模型
   ollama serve        # 启动服务
   ```

#### **代码实现**

```java
// 启动类
@SpringBootApplication
public class AiDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(AiDemoApplication.class, args);
    }
}

// 翻译接口
@RestController
@RequestMapping("/api")
public class TranslationController {

    private final OllamaChatModel ollamaChatModel;

    public TranslationController(OllamaChatModel ollamaChatModel) {
        this.ollamaChatModel = ollamaChatModel;
    }

    @PostMapping("/translate")
    public ResponseEntity<String> translateText(
            @RequestParam String text,
            @RequestParam(defaultValue = "法语") String language) {

        String systemPrompt = "你是一名专业翻译，保持术语准确，输出结果不带注释。";
        String userPrompt = String.format("将以下内容翻译为%s：%s", language, text);

        String fullPrompt = systemPrompt + "\n\n" + userPrompt;
        String result = ollamaChatModel.call(fullPrompt);

        return ResponseEntity.ok(result);
    }
}
```

#### **测试结果**

```bash
# 请求示例
curl -X POST http://localhost:8080/api/translate \
  -d "text=Spring AI极大地简化了大模型集成" \
  -d "language=英语"

# 响应输出
"Spring AI significantly simplifies the integration of large language models."
```

#### **技术扩展建议**

- **流式响应**：使用`StreamingChatClient`实现逐词输出。
- **上下文管理**：通过`ChatMemory`维护对话历史。
- **异常处理**：重试机制、Fallback降级策略。

---
