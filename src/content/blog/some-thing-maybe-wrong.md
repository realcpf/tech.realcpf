---
author: jiacheng.liu
pubDatetime: 2024-01-02T18:16:00.000Z
modDatetime:
title: 一些难以意识到的错误
featured: false
draft: false
tags:
  - tip
description: 关于一些难以意识到的错误
---

### M Mi K Ki G Gi

> 试试在各个操作系统上对文件大小单位的进制
> [https://physics.nist.gov/cuu/Units/binary.html](https://physics.nist.gov/cuu/Units/binary.html)

Prefixes for binary multiples
| **Factor** | **Name** | **Symbol** | **Origin** | **Derivation** |
| ---------- | -------- | ---------- | ------------------ | -------------- |
| 2^10 | kibi | Ki | kilobinary: (210)1 | kilo: (103)1 |
| 2^20 | mebi | Mi | megabinary: (210)2 | mega: (103)2 |
| 2^30 | gibi | Gi | gigabinary: (210)3 | giga: (103)3 |
| 2^40 | tebi | Ti | terabinary: (210)4 | tera: (103)4 |
| 2^50 | pebi | Pi | petabinary: (210)5 | peta: (103)5 |
| 2^60 | exbi | Ei | exabinary: (210)6 | exa: (103)6 |

| one **kibibit**  | 1 Kibit = 210 bit = **1024 bit**    |
| ---------------- | ----------------------------------- |
| one **kilobit**  | 1 kbit = 103 bit = **1000 bit**     |
| one **byte**     | 1 B = 23 bit = **8 bit**            |
| one **mebibyte** | 1 MiB = 220 B = **1 048 576 B**     |
| one **megabyte** | 1 MB = 106 B = **1 000 000 B**      |
| one **gibibyte** | 1 GiB = 230 B = **1 073 741 824 B** |
| one **gigabyte** | 1 GB = 109 B = **1 000 000 000 B**  |

在linux的coreutil 里同样采用了这套国际标准

[https://www.gnu.org/software/coreutils/manual/coreutils.html](https://www.gnu.org/software/coreutils/manual/coreutils.html)

With human-readable formats, output sizes are followed by a size letter such as ‘M’ for megabytes. BLOCK_SIZE=human-readable uses powers of 1024; ‘M’ stands for 1,048,576 bytes. BLOCK_SIZE=si is similar, but uses powers of 1000 and appends ‘B’; ‘MB’ stands for 1,000,000 bytes.

A block size specification preceded by ‘'’ causes output sizes to be displayed with thousands separators. The LC_NUMERIC locale specifies the thousands separator and grouping. For example, in an American English locale, ‘--block-size="'1kB"’ would cause a size of 1234000 bytes to be displayed as ‘1,234’. In the default C locale, there is no thousands separator so a leading ‘'’ has no effect.

An integer block size can be followed by a suffix to specify a multiple of that size. A bare size letter, or one followed by ‘iB’, specifies a multiple using powers of 1024. A size letter followed by ‘B’ specifies powers of 1000 instead. For example, ‘1M’ and ‘1MiB’ are equivalent to ‘1048576’, whereas ‘1MB’ is equivalent to ‘1000000’.

A plain suffix without a preceding integer acts as if ‘1’ were prepended, except that it causes a size indication to be appended to the output. For example, ‘--block-size="kB"’ displays 3000 as ‘3kB’.
