---
author: jiacheng.liu
pubDatetime: 2024-06-01T18:16:00.000Z
modDatetime:
title: argo 在数据任务编排执行中的应用
featured: false
draft: false
tags:
  - framework
description: 通过argo 使得数据ETL任务on cloud
---

### argo golang client

1. 带参数提交hello world

```go
var helloWorldWorkflow = wfv1.Workflow{
	ObjectMeta: metav1.ObjectMeta{
		GenerateName: "hello-world-with-param-",
	},
	Spec: wfv1.WorkflowSpec{
		Entrypoint: "whalesay",
		Templates: []wfv1.Template{
			{
				Name: "whalesay",
				Container: &corev1.Container{
					Image:   "docker/whalesay:latest",
					Command: []string{"cowsay"},
                    // 参数模板
					Args:    []string{"{{inputs.parameters.message}}"},
				},
				Inputs: wfv1.Inputs{
                    // 参数值 此处是固定值，还可以是 configMapKeyRef http等
					Parameters: []wfv1.Parameter{
						{
							Name:        "message",
							Value:       wfv1.AnyStringPtr("从golang传入的参数"),
							Description: wfv1.AnyStringPtr("这是描述"),
						},
					},
				},
			},
		},
	},
}
```

2. 访问外部服务的提交(先在argo下建立对应外部endpoint的service)

```yaml
kind: Endpoints
apiVersion: v1
metadata:
  name: outer-ip
subsets:
  - addresses:
      - ip: 192.168.124.23
    ports:
      - port: 2015
        name: tcp
---
apiVersion: v1
kind: Service
metadata:
  name: outer-ip
spec:
  ports:
    - protocol: TCP
      port: 2015
      name: tcp
      targetPort: 2015
```

> 这样就可以打通数据任务的网络连接

```go
var curlExtServiceWorkflow = wfv1.Workflow{
	ObjectMeta: metav1.ObjectMeta{
		GenerateName: "demo-ext-service-curl-",
	},
	Spec: wfv1.WorkflowSpec{
		Entrypoint: "curl",
		Templates: []wfv1.Template{
			{
				Name: "curl",
				Container: &corev1.Container{
					Image:   "docker.io/curlimages/curl",
					Command: []string{"curl"},
					Args:    []string{"{{inputs.parameters.addr}}"},
				},
				Inputs: wfv1.Inputs{
					Parameters: []wfv1.Parameter{
						{
							Name:        "addr",
							Value:       wfv1.AnyStringPtr("192.168.124.23:2015"),
							Description: wfv1.AnyStringPtr("这是描述"),
						},
					},
				},
			},
		},
	},
}

```
